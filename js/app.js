var url = 'https://pokeapi.co/api/v2/pokemon/';

// ------------- Print list of (20) Pokemons --------- //

var showPokemons = function (name, id) {
  var newDiv = $("<div class='item'>" + name + "</div>").attr("id", "item" + id);
  $("#content").append(newDiv);
};

// ------------- Print hidden cards with Pokemons description --------- //

var showHiddenInfo = function (info, id) {
  var newDiv = $("<div class='info'></div>");
  var newImg = $("<img class='info_img'>").attr('src', info.sprites.front_default);
  var newUl = $("<ul>");
  var newLi1 = $("<li class='pokemon_info'>Name: <span class='active'>" + info.name + "</span>");
  var types = info.types;
  var newLi2 = $("<li class='pokemon_info'>Type: </li>");
  for (var type of types) {
    var newSpan = $("<span class='active'>" + type.type.name + "/</span>");
    newLi2.append(newSpan);
  }
  var newLi3 = $("<li class='pokemon_info'>Height: <span class='active'>" + info.height + "</span>");
  var newLi4 = $("<li class='pokemon_info'>Weight: <span class='active'>" + info.weight + "</span>");

  newDiv.append(newImg);
  newDiv.append(newUl);
  newDiv.append(newLi1);
  newDiv.append(newLi2);
  newDiv.append(newLi3);
  newDiv.append(newLi4);
  newDiv.append(newLi4);
  $("#item" + id).append(newDiv);
}

// ------------- Get info from API for Hidden Cards --------- //

var getPokemonInfo = function (url) {
  return function (id) {
    var pokemonUrl = url + id;
    fetch(pokemonUrl)
      .then(function (response) {
        response = response.json();
        return response;
      }).then(function (pokemonInfo) {
      showHiddenInfo(pokemonInfo, id);
    })
  }
}

// ------------- Get from API Pokemons list --------- //

fetch(url)
  .then(function (response) {
  response = response.json();
  return response;
})
  .then(function (pokemons) {
    var pokemonList = pokemons.results;
    Promise.all(pokemonList).then(function (results) {
      for (var result of results) {
        var id = results.indexOf(result) + 1;
        showPokemons(result.name, id);
        var newPokemon = getPokemonInfo(url);
        newPokemon(id);
      }
    })
    .catch(function () {
    console.log("Can't show pokemons")
  });
  });

$("body").on("mouseup", function (e) {
  var target = $(".info");
  if (!target.is(e.target) && target.has(e.target).length === 0) {
    target.fadeOut("slow");
  }
});

$("body").on("click", ".item", function () {
  $(this).find(".info").fadeIn("slow");
});


